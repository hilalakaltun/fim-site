﻿using System.ComponentModel.DataAnnotations;

namespace FilmIzlemeSitesi.Models
{
    public class ExternalLoginConfirmationViewModel
    {
        [Required]
        [Display(Name = "Kullanıcı Adı")]
        public string UserName { get; set; }
    }

    public class ManageUserViewModel
    {
        [Required]
        [DataType(DataType.Password)]
        [Display(Name = "Mevcut Şİfre")]
        public string OldPassword { get; set; }

        [Required]
        [StringLength(100, ErrorMessage = "Şifre 6 karakterden az olamaz", MinimumLength = 6)]
        [DataType(DataType.Password)]
        [Display(Name = "Yeni Şifre")]
        public string NewPassword { get; set; }

        [DataType(DataType.Password)]
        [Display(Name = "Onayla")]
        [Compare("NewPassword", ErrorMessage = "Yeni şifre ve onay şifre eşleşmiyor.")]
        public string ConfirmPassword { get; set; }
    }

    public class LoginViewModel
    {
        [Required]
        [Display(Name = "Kullanıcı Adı")]
        public string UserName { get; set; }

        [Required]
        [DataType(DataType.Password)]
        [Display(Name = "Şifre")]
        public string Password { get; set; }

        [Display(Name = "Şifreni unuttun mu?")]
        public bool RememberMe { get; set; }
    }

    public class RegisterViewModel
    {
        [Required]
        [Display(Name = "Kullanıcı Adı")]
        public string UserName { get; set; }

        [Required]
        [StringLength(100, ErrorMessage = "Şifre az 6 karakterli olmalı.", MinimumLength = 6)]
        [DataType(DataType.Password)]
        [Display(Name = "Şifre")]
        public string Password { get; set; }

        [DataType(DataType.Password)]
        [Display(Name = "Onayla")]
        [Compare("Password", ErrorMessage = "Yeni şifre ve onay şifre eşleşmiyor.")]
        public string ConfirmPassword { get; set; }
    }
}
