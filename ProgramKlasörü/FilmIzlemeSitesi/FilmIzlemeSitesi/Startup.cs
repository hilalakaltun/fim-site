﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(FilmIzlemeSitesi.Startup))]
namespace FilmIzlemeSitesi
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
