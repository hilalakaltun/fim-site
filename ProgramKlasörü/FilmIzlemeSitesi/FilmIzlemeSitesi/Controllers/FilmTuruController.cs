﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using FilmIzlemeSitesi.Models;

namespace FilmIzlemeSitesi.Controllers
{
    public class FilmTuruController : Controller
    {
        private FilmIzleEntities db = new FilmIzleEntities();

        // GET: /FilmTuru/
        public ActionResult Index()
        {
            return View(db.FilmTuruTablosu.ToList());
        }

        // GET: /FilmTuru/Details/5

        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            FilmTuruTablosu filmturutablosu = db.FilmTuruTablosu.Find(id);
            if (filmturutablosu == null)
            {
                return HttpNotFound();
            }
            return View(filmturutablosu);
        }

        // GET: /FilmTuru/Create
    [Authorize(Users = "admin")]
        public ActionResult Create()
        {
            return View();
        }

        // POST: /FilmTuru/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include="FilmTuruId,FilmTuru")] FilmTuruTablosu filmturutablosu)
        {
            if (ModelState.IsValid)
            {
                db.FilmTuruTablosu.Add(filmturutablosu);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(filmturutablosu);
        }

        // GET: /FilmTuru/Edit/5
        [Authorize(Users = "admin")]
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            FilmTuruTablosu filmturutablosu = db.FilmTuruTablosu.Find(id);
            if (filmturutablosu == null)
            {
                return HttpNotFound();
            }
            return View(filmturutablosu);
        }

        // POST: /FilmTuru/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include="FilmTuruId,FilmTuru")] FilmTuruTablosu filmturutablosu)
        {
            if (ModelState.IsValid)
            {
                db.Entry(filmturutablosu).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(filmturutablosu);
        }

        // GET: /FilmTuru/Delete/5
        [Authorize(Users = "admin")]
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            FilmTuruTablosu filmturutablosu = db.FilmTuruTablosu.Find(id);
            if (filmturutablosu == null)
            {
                return HttpNotFound();
            }
            return View(filmturutablosu);
        }

        // POST: /FilmTuru/Delete/5
        [Authorize(Users = "admin")]
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            FilmTuruTablosu filmturutablosu = db.FilmTuruTablosu.Find(id);
            db.FilmTuruTablosu.Remove(filmturutablosu);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
