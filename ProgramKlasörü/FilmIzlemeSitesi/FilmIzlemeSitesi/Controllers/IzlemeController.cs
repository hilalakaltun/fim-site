﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using FilmIzlemeSitesi.Models;

namespace FilmIzlemeSitesi.Controllers
{
    public class IzlemeController : Controller
    {
        private FilmIzleEntities db = new FilmIzleEntities();

        // GET: /Izleme/
        public ActionResult Index()
        {
            var harekettablosu = db.HareketTablosu.Include(h => h.AspNetUsers).Include(h => h.FilmTablosu);
            return View(harekettablosu.ToList().FindAll(p => p.HareketTipi == HareketTipi.FilmIzleme.ToString()));
        }

        // GET: /Izleme/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            HareketTablosu harekettablosu = db.HareketTablosu.Find(id);
            if (harekettablosu == null)
            {
                return HttpNotFound();
            }
            return View(harekettablosu);
        }

        // GET: /Izleme/Create
        public ActionResult Create()
        {
            ViewBag.KullaniciId = new SelectList(db.AspNetUsers, "Id", "UserName");
            ViewBag.FilmId = new SelectList(db.FilmTablosu, "FilmId", "FilmAdi");
            return View();
        }

        // POST: /Izleme/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include="HareketId,KullaniciId,FilmId,HareketTipi,HareketTarihi")] HareketTablosu harekettablosu)
        {
            if (ModelState.IsValid)
            {
                harekettablosu.HareketTipi = HareketTipi.FilmIzleme.ToString();
                harekettablosu.HareketTarihi = DateTime.Now;
                db.HareketTablosu.Add(harekettablosu);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.KullaniciId = new SelectList(db.AspNetUsers, "Id", "UserName", harekettablosu.KullaniciId);
            ViewBag.FilmId = new SelectList(db.FilmTablosu, "FilmId", "FilmAdi", harekettablosu.FilmId);
            return View(harekettablosu);
        }

        // GET: /Izleme/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            HareketTablosu harekettablosu = db.HareketTablosu.Find(id);
            if (harekettablosu == null)
            {
                return HttpNotFound();
            }
            ViewBag.KullaniciId = new SelectList(db.AspNetUsers, "Id", "UserName", harekettablosu.KullaniciId);
            ViewBag.FilmId = new SelectList(db.FilmTablosu, "FilmId", "FilmAdi", harekettablosu.FilmId);
            return View(harekettablosu);
        }

        // POST: /Izleme/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include="HareketId,KullaniciId,FilmId,HareketTipi,HareketTarihi")] HareketTablosu harekettablosu)
        {
            if (ModelState.IsValid)
            {
                db.Entry(harekettablosu).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.KullaniciId = new SelectList(db.AspNetUsers, "Id", "UserName", harekettablosu.KullaniciId);
            ViewBag.FilmId = new SelectList(db.FilmTablosu, "FilmId", "FilmAdi", harekettablosu.FilmId);
            return View(harekettablosu);
        }

        // GET: /Izleme/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            HareketTablosu harekettablosu = db.HareketTablosu.Find(id);
            if (harekettablosu == null)
            {
                return HttpNotFound();
            }
            return View(harekettablosu);
        }

        // POST: /Izleme/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            HareketTablosu harekettablosu = db.HareketTablosu.Find(id);
            db.HareketTablosu.Remove(harekettablosu);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
