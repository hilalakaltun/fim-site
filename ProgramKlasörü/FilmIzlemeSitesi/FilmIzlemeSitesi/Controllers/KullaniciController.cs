﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using FilmIzlemeSitesi.Models;

namespace FilmIzlemeSitesi.Controllers
{
    public class KullaniciController : Controller
    {
        private FilmIzleEntities db = new FilmIzleEntities();

        // GET: /Kullanici/
        public ActionResult Index()
        {
            return View(db.AspNetUsers.ToList());
        }

        // GET: /Kullanici/Details/5
        public ActionResult Details(string id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            AspNetUsers aspnetusers = db.AspNetUsers.Find(id);
            if (aspnetusers == null)
            {
                return HttpNotFound();
            }
            return View(aspnetusers);
        }

        // GET: /Kullanici/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: /Kullanici/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include="Id,UserName,PasswordHash,SecurityStamp,Discriminator")] AspNetUsers aspnetusers)
        {
            if (ModelState.IsValid)
            {
                db.AspNetUsers.Add(aspnetusers);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(aspnetusers);
        }

        // GET: /Kullanici/Edit/5
        public ActionResult Edit(string id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            AspNetUsers aspnetusers = db.AspNetUsers.Find(id);
            if (aspnetusers == null)
            {
                return HttpNotFound();
            }
            return View(aspnetusers);
        }

        // POST: /Kullanici/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include="Id,UserName,PasswordHash,SecurityStamp,Discriminator")] AspNetUsers aspnetusers)
        {
            if (ModelState.IsValid)
            {
                db.Entry(aspnetusers).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(aspnetusers);
        }

        // GET: /Kullanici/Delete/5
        public ActionResult Delete(string id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            AspNetUsers aspnetusers = db.AspNetUsers.Find(id);
            if (aspnetusers == null)
            {
                return HttpNotFound();
            }
            return View(aspnetusers);
        }

        // POST: /Kullanici/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(string id)
        {
            AspNetUsers aspnetusers = db.AspNetUsers.Find(id);
            db.AspNetUsers.Remove(aspnetusers);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
