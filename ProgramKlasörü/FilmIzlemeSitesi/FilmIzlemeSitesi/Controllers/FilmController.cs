﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using FilmIzlemeSitesi.Models;

namespace FilmIzlemeSitesi.Controllers
{
    public class FilmController : Controller
    {
        private FilmIzleEntities db = new FilmIzleEntities();

        // GET: /Film/
        public ActionResult Index()
        {
            var filmtablosu = db.FilmTablosu.Include(f => f.FilmTuruTablosu);
            return View(filmtablosu.ToList());
        }

        // GET: /Film/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            FilmTablosu filmtablosu = db.FilmTablosu.Find(id);
            if (filmtablosu == null)
            {
                return HttpNotFound();
            }
            return View(filmtablosu);
        }

        // GET: /Film/Create
        [Authorize (Users="admin")]
        public ActionResult Create()
        {
            ViewBag.FilmTuruId = new SelectList(db.FilmTuruTablosu, "FilmTuruId", "FilmTuru");
            return View();
        }

        // POST: /Film/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        [Authorize(Users = "admin")]
        public ActionResult Create([Bind(Include="FilmId,FilmAdi,Yonetmen,FilmTuruId,ImdbPuani,CikisTarihi")] FilmTablosu filmtablosu)
        {
            if (ModelState.IsValid)
            {
                db.FilmTablosu.Add(filmtablosu);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.FilmTuruId = new SelectList(db.FilmTuruTablosu, "FilmTuruId", "FilmTuru", filmtablosu.FilmTuruId);
            return View(filmtablosu);
        }

        // GET: /Film/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            FilmTablosu filmtablosu = db.FilmTablosu.Find(id);
            if (filmtablosu == null)
            {
                return HttpNotFound();
            }
            ViewBag.FilmTuruId = new SelectList(db.FilmTuruTablosu, "FilmTuruId", "FilmTuru", filmtablosu.FilmTuruId);
            return View(filmtablosu);
        }

        // POST: /Film/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [Authorize(Users = "admin")]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include="FilmId,FilmAdi,Yonetmen,FilmTuruId,ImdbPuani,CikisTarihi")] FilmTablosu filmtablosu)
        {
            if (ModelState.IsValid)
            {
                db.Entry(filmtablosu).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.FilmTuruId = new SelectList(db.FilmTuruTablosu, "FilmTuruId", "FilmTuru", filmtablosu.FilmTuruId);
            return View(filmtablosu);
        }

        // GET: /Film/Delete/5
        [Authorize(Users = "admin")]
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            FilmTablosu filmtablosu = db.FilmTablosu.Find(id);
            if (filmtablosu == null)
            {
                return HttpNotFound();
            }
            return View(filmtablosu);
        }

        // POST: /Film/Delete/5

        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        [Authorize(Users = "admin")]

        public ActionResult DeleteConfirmed(int id)
        {
            FilmTablosu filmtablosu = db.FilmTablosu.Find(id);
            db.FilmTablosu.Remove(filmtablosu);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
